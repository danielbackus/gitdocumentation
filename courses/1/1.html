<html>

<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" />
    <link rel="stylesheet" href="../../css/main.css" />
</head>

<body>
    <div class="container">
        <div>
            <h2>Basic Git Workflow Tutorial</h2>
            <p>This tutorial will cover the basics of Git workflows, including creating a new repository, committing, branching,
                merging branches, and resolving conflicts that arise while merging branches.</p>

            <h3>Contents</h3>
            <ul>
                <a href="#setup">
                    <li>Getting Started</li>
                </a>
                <a href="#branch">
                    <li>Branching</li>
                </a>
                <a href="#merge">
                    <li>Merging Branches</li>
                </a>
                <a href="#conflict">
                    <li>Merging Conflict</li>
                </a>
            </ul>
        </div>
        <div>
            <a id="setup">
                <h3>Getting Started</h3>
            </a>

            <p>In the follow-along video, I'll be using a Windows PC, but all software you need for this tutorial is also available
                for Linux and Mac.</p>
            <p>For purposes of this demonstration, you will need the following software installed:</p>
            <ul>
                <li>
                    <a href="https://code.visualstudio.com/">Visual Studio Code</a> (or another IDE of choice)</li>
                <li>
                    <a href="https://git-scm.com/downloads">Git</a>
                </li>
            </ul>
            <p></p>
            <p>Now, create a new Bitbucket repository named
                <strong>git_workflow_demo</strong> through the UI at
                <a href="https://bitbucket.org/repo/create">Bitbucket</a>.</p>
            <p>Now we're going to configure our local Git identity to save future headaches, and clone our new remote repository
                on Bitbucket.</p>
            <div class="code bash">
                git config --global user.email "your.email@example.com"
                <br /> git config --global user.name "Your Name"
                <br /> git clone https://bitbucket.org/your_username/git_workflow_demo
            </div>
            <p>Depending on your platform, you should see a dialogue prompting you to login to your Atlassian/Bitbucket account
                that looks something like the following:</p>
            <img src="./images/atlassian_login.png" />
            <p>Once you've entered your password here, you should remain authenticated for the duration of the session. Now
                that you're authenticated, let's go back to our Bash session, which should have finished cloning our empty
                repository.
            </p>
            <div class="code bash">
                cd git_workflow_demo
                <br /> code index.js
            </div>
            <p>This will launch VS Code and create a new file named
                <span class="file">index.js</span>, which we'll fill with some placeholder Javascript.</p>
            <p>Note: If you're using another IDE, you can launch that here instead. The important part is creating a file with
                the correct name, in this case,
                <span class="file">index.js</span>.</p>
            <div class="code js">
                module.exports = {};
            </div>
            <p>That's all the code we need for now, so you can save the file and go back to your Bash terminal window.</p>
            <p>If you're familiar with Javascript but haven't seen this
                <span class="item">module.exports</span> object before, don't worry to much about it. It's a
                <a href="http://requirejs.org/docs/commonjs.html">CommonJS</a> syntax to package Javascript modules for non-browser ecosystems, most typically
                <a href="https://nodejs.org/en/">NodeJS</a>. Now let's stage this new file and commit it to our repository.</p>
            <div class="code bash">
                git add index.js
                <br />git commit -m 'Initial commit'
                <br/>git push
            </div>
            <p>Now if we look at our repository on Bitbucket, we'll find that it has one commit, authored by us, with one file
                named
                <span class="file">index.js</span>.</p>
            <p>OK, so let's continue setting up our repository as we prepare to begin our development cycle.</p>
        </div>
        <div>
            <a id="branch">
                <h3>Branching</h3>
            </a>
            <p>Now we're going to branch from master. It's a good time to overview Git branching strategies.</p>
            <p>I've included here a branching strategy presented by Vincent Dressien in an
                <a href="http://nvie.com/posts/a-successful-git-branching-model/">excellent article</a> on the topic.</p>
            <img src="./images/git_model.png" width="575" />
            <p>We'll be using a modified variant of this branching strategy for the purposes of this demonstration, so let's
                get started.</p>
            <div class="code">
                git branch dev
                <br /> git checkout dev
                <br /> git push -u origin dev
            </div>
            <p>These three commands:
                <ul>
                    <li>Created a new branch named
                        <span class="item">dev</span> in your local repository.</li>
                    <li>Checked out the
                        <span class="item">dev</span> branch.</li>
                    <li>Specified a corresponding branch also named
                        <span class="item">dev</span> on the remote Bitbucket repository (origin) and pushed changes there.</li>
                </ul>
            </p>
            <p>Our feature branches will be grouped by sprint/release, so let's create a corresponding branch for the current
                sprint in the exact same way.
            </p>
            <div class="code">
                git branch sprint1
                <br /> git checkout sprint1
                <br /> git push -u origin sprint1
            </div>
        </div>
        <div>
            <a id="merge">
                <h3>Merging Branches</h3>
            </a>
            <p>So let's finally get to creating a new feature. First, we'll repeat the branch process once more to create a
                branch to house our feature. </p>
            <div class="code">
                git branch feature1
                <br /> git checkout feature1
                <br /> git push -u origin feature1
            </div>
            <p>Are you tired of creating branches yet? Well, I'm sorry, but we're going to create one more, which we'll use
                in the next section to showcase resolving merge conflicts.</p>
            <div class="code">
                git branch feature2
                <br />git checkout feature2
                <br/>git push -u origin feature2
            </div>
            <p>OK, all done branching for now. Let's checkout our
                <span class="item">feature1</span> branch and actually implement our feature: we'll just write a function to sum numbers.</p>
            <div class="code bash">
                git checkout feature1
                <br />code add.js
            </div>
            <p>Now in our IDE, we'll write some simple Javascript to sum numbers.</p>
            <div class="code js">module.exports = function add() {
                <br/>
                <span class="tab1">var total = arguments[0];</span>
                <br/>
                <span class="tab1">for (var i = 1; i &lt; arguments.length; i++)</span>
                <br/>
                <span class="tab2">total += arguments[i];</span>
                <br/>
                <span class="tab1">}</span>
                <br/>
                <span class="tab1">return total;</span>
                <br/>};
            </div>
            <p>Good enough for now, back to our bash terminal.</p>
            <p>Now let's just add a reference to this function in our main file.</p>
            <div class="code">code index.js</div>
            <div class="code">
                module.exports = {
                <br/>
                <span class="tab1">add: require('./add')</span>
                <br/> };
            </div>
            <p>OK, great, this feature is all done! Let's stage our changes to these two files, commit them to our feature branch,
                and push the commit to the remote repository.</p>
            <div class="code">
                git add add.js
                <br/>git add index.js
                <br/>git commit -m 'Added addition function'
                <br/>git push
            </div>
            <p>Great! Now let's create a pull request to merge our feature branch into the release branch.</p>
            <p>Navigate to your repo's Pull Requests via Bitbucket's UI or use the following
                <a href="https://bitbucket.org/your_username/git_workflow_demo/pull-requests/new">link</a> and change the
                <span class="item">your_username</span> token to your actual username.
            </p>
            <p>We want to merge feature1 (our current feature branch) into sprint1 (the release branch).</p>
            <img src="./images/pull_request1-1.png" />
            <p>Normally, we'd have a peer review our code before merging it into a release, but for the purposes of this demonstration,
                we'll just hit the Merge button.</p>
            <img src="./images/pull_request1-2.png" />
            <p>Confirm that everything looks good, and merge it in.</p>
            <img src="./images/pull_request1-3.png" />
            <p>Note that this merge was able to complete automatically, because there were no merge conflicts. That usually
                won't be the case, so we'll explore that in the next section.</p>
            <p>Lastly, before we go on to anything else, it's worth noting that our local repository's
                <span class="item">sprint1</span> branch is behind the remote after the merge. Let's take care of that now before we forget.
                Since the remote branch is ahead, we'll be using
                <span class="item">git pull</span> here.</p>
            <div class="code">
                git checkout sprint1
                <br />git pull
            </div>
        </div>
        <div>
            <a id="conflict">
                <h3>Merge Conflicts</h3>
            </a>
            <p>Now, we're going to create another feature branch to simulate parallel development. If you're thinking ahead,
                you can foresee that we will have a merge conflict down the road when we try to merge this new feature.</p>
            <p>OK, let's charge ahead with development of this feature.</p>
            <div class="code">git checkout feature2
                <br/>code subtract.js
            </div>
            <div class="code">
                module.exports = function subtract() {
                <br/>
                <span class="tab1">var total = arguments[0];</span>
                <br/>
                <span class="tab1">for (var i = 1; i &lt; arguments.length; i++ {</span>
                <br/>
                <span class="tab2">total -= arguments[i];</span>
                <br/>
                <span class="tab1">}</span>
                <br/>
                <span class="tab1">return total;</span>
                <br/>};
            </div>
            <p>Save our changes and go back to our bash terminal.</p>
            <div class="code">code index.js</div>
            <p>Now, we'll want to create a reference to our new codefile from the entrypoint.</p>
            <div class="code">
                module.exports = {
                <br/>
                <span class="tab1">subtract: require('./subtract')</span>
                <br/> };
            </div>
            <p>OK, we now have a subtract feature coded. Let's save it and go back to our bash terminal to stage and commit
                these changes.
            </p>
            <div class="code">
                git add subtract.js
                <br/>git add index.js
                <br/>git commit -m 'Added subtraction function'
                <br/>git push
            </div>
            <p>OK, these changes look good, let's get ready to merge them to the release branch with a pull request.</p>
            <p>Again, we'll navigate to your repo's Pull Requests via Bitbucket's UI or use the following
                <a href="https://bitbucket.org/your_username/git_workflow_demo/pull-requests/new">link</a> and change the
                <span class="item">your_username</span> token to your actual username.
            </p>
            <p>We want to merge feature2 (our current feature branch) into sprint1 (the release branch).</p>
            <img src="./images/pull_request2-1.png" />
            <p>Again, we'd typically have a peer review our code before merging it into a release, but for the purposes of this
                demonstration, we'll be skipping that step.</p>
            <img src="./images/pull_request2-2.png" />
            <p>You can see that Bitbucket has flagged a merge conflict that we're going to need to resolve. If we hit the Merge
                button, we can't proceed. </p>
            <img src="./images/pull_request2-3.png" />
            <p>Let's walk through manually merging this branch.</p>
            <div class="code">
                git checkout sprint1
                <br/>git merge feature2
            </div>
            <p>
                First, we checkout
                <span class="item">sprint1</span>, the destination branch and then use the
                <span class="item">git merge</span> command on the source branch,
                <span class="item">feature2</span>. This command, will throw an warning as follows.
            </p>
            <div class="code">
                Auto-merging index.js
                <br/>CONFLICT (content): Merge conflict in index.js
                <br/>Automatic merge failed; fix conflicts and then commit the result.
            </div>
            <div class="code">code index.js</div>
            <p>
                Once you open
                <span class="item">index.js</span>, you will see both versions of the file one after the other, like so:
            </p>
            <div class="code">module.exports = {
                <br/> &lt;&lt;&lt;&lt;&lt;&lt;&lt; HEAD
                <br/>
                <span class="tab1">add: require('./add')</span>
                </span>
                <br/>=======
                <br/>
                <span class="tab1">
                    subtract: require('./subtract')
                </span>
                <br/>};
                <br/>>>>>>>> feature2
            </div>
            <p>Update the code as follows to include references to both code files.</p>
            <div class="code">
                module.exports = {
                <br/>
                <span class="tab1">add: require('./add'),</span>
                <br/>
                <span class="tab1">subtract: require('./subtract')</span>
                <br/> };
            </div>
            <p>Now we can commit these changes to complete the merge and push them to the remote repo.</p>
            <div class="code">
                git add index.js
                <br/> git add subtract.js
                <br/> git commit -m 'Resolved merge conflict manually'
                <br/> git push
            </div>
            <p>Unlike last time, because we did the merge locally, instead of on the remote repo via Bitbucket's UI, our local
                respository is entirely up-to-date.</p>
        </div>

    </div>
    </div>
</body>

</html>